FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-12

USER root

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install cmake==3.18.4

COPY start-singleuser.sh /usr/local/bin/

RUN mamba env create -f /builds/k8s-let/notebooks/jh-notebook-gene-technology/environment.yml && \
  source activate altlabs && \
  python -m ipykernel install --name=altlabs --display-name='Gene Technology'
  

USER 1000
